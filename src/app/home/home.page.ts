import { Component, NgZone } from '@angular/core';
import { BioCatch } from '@ionic-native/biocatch/ngx';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public isOn = false;
  public isPaused = false;
  public customerSessionID = 'bc-test-1';
  public contextName = 'LOGIN_1';

  constructor(private ngZone: NgZone, private bioCatch: BioCatch, private toastController: ToastController) {
  }

  public async start() {
    const wupUrl = 'https://wup-4ff4f23f.eu.v2.we-stats.com/client/v3/web/wup?cid=dummy';

    await this.bioCatch.start(this.customerSessionID, wupUrl, null);

    this.ngZone.run(() => this.isOn = true);
    this.presentToast('Data collection started!');
  }

  public async stop() {
    await this.bioCatch.stop();

    this.ngZone.run(() => this.isOn = false);
    this.presentToast('Data collection stopped.');
  }

  public async pause() {
    await this.bioCatch.pause();

    this.ngZone.run(() => this.isPaused = true);
    this.presentToast('Data collection paused.');
  }

  public async resume() {
    await this.bioCatch.resume();

    this.ngZone.run(() => this.isPaused = false);
    this.presentToast('Data collection resumed!');
  }

  public async resetSession() {
    await this.bioCatch.resetSession();

    this.presentToast('Session reset.');
  }

  public async changeContext() {
    await this.bioCatch.changeContext(this.contextName);

    this.presentToast(`Context changed to: ${this.contextName}`);
  }

  public async updateCustomerSessionID() {
    await this.bioCatch.updateCustomerSessionID(this.customerSessionID);

    this.presentToast(`Customer session ID updated to: ${this.customerSessionID}`);
  }

  presentToast(message: string) {
    this.toastController.create({message, duration: 1500, color: 'medium'}).then(toast => toast.present());
  }
}
